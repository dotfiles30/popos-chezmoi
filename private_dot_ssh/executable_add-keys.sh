#!/bin/bash

for k in ~/.ssh/*.pub; 
  chmod 600 "${k%.pub}";
  chmod 600 "${k%}";
  do ssh-add "${k%}";
done
