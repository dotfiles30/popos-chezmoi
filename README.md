# 🚀 Install
1. Install [Chezmoi](https://www.chezmoi.io/install/)
2. `chezmoi init --apply https://gitlab.com/dotfiles30/popos-chezmoi.git`
3. Add private `key.txt.age`
4. `chezmoi apply`
5. Change remote to ssh url in the `.git/config` after setting up to have passwordless git actions

---
- [Linux readme](readme-linux.md)
- [Windows readme](readme-windows.md)

