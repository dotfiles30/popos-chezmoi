# Always forget
- How to check an alias for a cmdlet?
```bash
Get-Alias -Definition [cmdlet]
gal -D [cmdlet]
```
- Copy to clipboard
```bash
echo "some shit" | Set-Clipboard
echo "some shit" | scb
```

# Powershell config
- Location: Documents/Powershell
- Accessible with `nvim $PROFILE`
- I like to source my `.config/powershell/profile.ps1` file from here

---

https://hamidmosalla.com/2022/12/26/how-to-customize-windows-terminal-and-powershell-using-fzf-neovim-and-beautify-it-with-oh-my-posh/
