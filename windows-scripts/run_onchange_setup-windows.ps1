# vim: filetype=ps1

# In case u have bad installation like "bad hash checking or smth": scoop cache rm git; scoop uninstall git; scoop install git

$programs = @(
  # ------ Terminal ------
  "pwsh",
  "zoxide",
  "bat",
  "JetBrainsMono-NF",
  "nonportable/workman-np",
  "eza",
  "glazewm",
  "nvm",
  "chezmoi",
  "neovim", "gcc", "neovide", "vcredist2022", "ripgrep", 

  # A fast and powerful alternative to grep.
  "sift",
  "tldr",
  "unzip",
  "extras/upscayl",
  "extras/ayugram",

  # Universal archives extractor.
  "unar",
  
  # Differencing and merging too
  "winmerge"

  "lazygit", "fd", "fzf",
  # ------ Minimum ------
  "sophiapp",
  "sudo",
  "vivaldi",
  "file-converter-np",
  "obsidian",
  "bulk-crap-uninstaller",
  "fsviewer",
  "stretchly",
  "anki",
  "windirstat",

  # A hardware monitoring program that reads PC systems main health sensors 
  # : voltages, temperatures, fans speed.
  "hwmonitor",
  "sharex",
  "qbittorrent",
  "context-menu-manager",
  "flux",
  "capture2text",
  "sumatrapdf",
  "vlc",
  "peazip",
  "main/espanso",

  # CPU temperatures
  "coretemp",
  "discord"
);
# ------ MISC ------
# electerm,
# wingetui,
# mcomix,
# kdenlive,
# digicam,
# flameshot,
# autohotkey,
# gittyup,
# obs-studio,
# czkawka,
# zotero,

# Versatile, classic, complete name server software.
# bind,

# The simple countdown timer for Windows
# hourglass,
# okular,
# firefox,
# paint.net,
# fork,
# tor-browser,
# thunderbird,
# caesium-image-compressor,
# virtualbox-with-extension-pack-np,
# calibre,
# blender,

# A high-precision scientific calculator featuring a fast,
# keyboard-driven user interface.
# speedcrunch,

# deepl;


sudo scoop install $programs;


# TODO https://developer.hashicorp.com/vagrant
# TODO use peaces of this https://github.com/cocreators-ee/aperture-control with chezmoi
#https://getcoldturkey.com/features/
# TO get more https://www.softpedia.com


# ---------------------------
# Setup node & npm
# ---------------------------
if (Get-Command "node" -errorAction SilentlyContinue){}
else {
  sudo nvm install latest
  sudo nvm use latest
}
