# Related to i3 configuration
- Don't create new mate's panels. They will cover up window manager for some reason and the solution to that is reset mate's panels by right clicking on the main one

## Dconf setup 
`org > mate > desktop > session`

1. `required-components-list` was:
```
['windowmanager', 'panel', 'filemanager', 'dock']
```
and become:
```
['windowmanager', 'panel']
```
2. ` > required-components > windowmanager` was: `marco` and become: `i3`



# Gotchas
- First of all, always update the system 
```bash
sudo apt-get update && sudo apt-get upgrade && sudo apt-get dist-upgrade
```
- `OhMyBash` installation rewrites `~/.bashrc`
- `Fish` don't support bash syntax (`set` instead of `=` etc)
- After telegram installation from Flatpak, sometimes popOS shortcut can be broken
Solved with the following from [here](https://github.com/telegramdesktop/tdesktop/issues/27550#issuecomment-2003118493)
```bash
systemctl reload --user dbus-broker.service
```
# Always forget
```bash
# rerun last command
!!
sudo!!
!-1

# run n-th command from history
!n

# change default terminal emulator
sudo update-alternatives --config x-terminal-emulator

# get os info
cat /etc/os-release

# find all files in directory and its subdirectory
find | grep "regex here"
# next you can do stuff with found files using pipes or xargs
```

## APT
Sources located in `/etc/apt/sources.list`

```bash
sudo apt-get autoremove
sudo apt-get autoclean
sudo apt clean # clean the cache
sudo do-release-upgrade # Reboot your system and then download and upgrade your distro
apt show <program> # get program's info
```

The `--purge` option in the `apt` command is used to remove packages from your system along with their configuration files

## SystemD
Systemd expects all configuration files to be put under `/etc/systemd/system/`

```bash
# list running services 
systemctl list-units --type=service --state=running

# reload the configuration files
sudo systemctl daemon-reload
```

## System
```bash
# list users on the machine
awk -F':' '{ print $1}' /etc/passwd
```
